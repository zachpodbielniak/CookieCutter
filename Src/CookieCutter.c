/*
  ____            _    _       ____      _   _            
 / ___|___   ___ | | _(_) ___ / ___|   _| |_| |_ ___ _ __ 
| |   / _ \ / _ \| |/ / |/ _ \ |  | | | | __| __/ _ \ '__|
| |__| (_) | (_) |   <| |  __/ |__| |_| | |_| ||  __/ |   
 \____\___/ \___/|_|\_\_|\___|\____\__,_|\__|\__\___|_|  

Tool Suite For Creating Project Templates.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <jansson.h>
#include <CRock/CRock.h>
#include <PodNet/CProcess/CProcess.h>




typedef struct __STATE
{
	HANDLE 		hHttpServer;
	
	DLPSTR 		dlpszBindAddresses;
	LPUSHORT 	lpusBindPorts;

	LONG 		lArgCount;
	DLPSTR 		dlpszArgValues;
} STATE, *LPSTATE;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__CookieIsSafe(
	_In_ 		LPCSTR 			lpcszCookie
){
	EXIT_IF_UNLIKELY_NULL(lpcszCookie, FALSE);

	LPVOID lpExists;
	
	lpExists = StringInString(lpcszCookie, "..");
	if (NULLPTR != lpExists)
	{ return FALSE ;}

	return TRUE;
}




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE
__Root(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	CSTRING csBody[8192];
	ZeroMemory(csBody, sizeof(csBody));

	StringPrintFormatSafe(
		csBody,
		sizeof(csBody) - 1,
		"<html><head><title>CookieCutter</title></head><body>"
		"<h1>Welcome To CookieCutter</h1>"
		"</body></html>"
	);

	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");
	RequestAddResponse(lprdData->hRequest, csBody, StringLength(csBody));

	return HTTP_STATUS_200_OK;
}




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE
__Project(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hProcess;
	LPFORM_DATA lpfdLicense;
	LPFORM_DATA lpfdServer;
	LPFORM_DATA lpfdGroup;
	LPFORM_DATA lpfdProject;
	LPFORM_DATA lpfdCookie;
	LPFORM_DATA lpfdDescription;
	LPFORM_DATA lpfdReadMe;
	UARCHLONG ualIndex;
	CSTRING csResponse[512];
	CSTRING csProcess[512];
	CSTRING csProcessArgs[8192];

	ZeroMemory(csResponse, sizeof(csResponse));
	ZeroMemory(csProcess, sizeof(csProcess));
	ZeroMemory(csProcessArgs, sizeof(csProcessArgs));

	lpfdLicense = NULLPTR;
	lpfdServer = NULLPTR;
	lpfdGroup = NULLPTR;
	lpfdProject = NULLPTR;
	lpfdGroup = NULLPTR;
	lpfdCookie = NULLPTR;
	lpfdDescription = NULLPTR;
	lpfdReadMe = NULLPTR;


	if (FALSE == PostIsMultipartForm(lprdData->hRequest))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	if (FALSE == ProcessMultipartForm(lprdData->hRequest))
	{ return HTTP_STATUS_400_BAD_REQUEST; }


	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvFormData);
		ualIndex++
	){
		LPFORM_DATA lpfdTemp;

		lpfdTemp = VectorAt(lprdData->hvFormData, ualIndex);
		if (NULLPTR == lpfdTemp)
		{ continue; }

		if (0 == StringCompare(lpfdTemp->lpszKey, "License"))
		{ lpfdLicense = lpfdTemp; }
		else if (0 == StringCompare(lpfdTemp->lpszKey, "Server"))
		{ lpfdServer = lpfdTemp; }
		else if (0 == StringCompare(lpfdTemp->lpszKey, "Group"))
		{ lpfdGroup = lpfdTemp; }
		else if (0 == StringCompare(lpfdTemp->lpszKey, "Project"))
		{ lpfdProject = lpfdTemp; }
		else if (0 == StringCompare(lpfdTemp->lpszKey, "Cookie"))
		{ lpfdCookie = lpfdTemp; }
		else if (0 == StringCompare(lpfdTemp->lpszKey, "Description"))
		{ lpfdDescription = lpfdTemp; }
		else if (0 == StringCompare(lpfdTemp->lpszKey, "ReadMe"))
		{ lpfdReadMe = lpfdTemp; }
		else 
		{ return HTTP_STATUS_400_BAD_REQUEST; }
	}

	if (
		NULLPTR == lpfdLicense ||
		NULLPTR == lpfdServer ||
		NULLPTR == lpfdGroup ||
		NULLPTR == lpfdProject ||
		NULLPTR == lpfdCookie ||
		NULLPTR == lpfdDescription ||
		NULLPTR == lpfdReadMe
	){ return HTTP_STATUS_400_BAD_REQUEST; }

	/* Is Cookie Path Safe? */
	if (FALSE == __CookieIsSafe((LPCSTR)lpfdCookie->lpszValue))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	/* 
	PrintFormat(
		"License: %s\nServer %s\nGroup: %s\nProject: %s\nCookie: %s\nDescription: %s\nReadMe: %s\n",
		lpfdLicense->lpszValue,
		lpfdServer->lpszValue,
		lpfdGroup->lpszValue,
		lpfdProject->lpszValue,
		lpfdCookie->lpszValue,
		lpfdDescription->lpszValue,
		lpfdReadMe->lpszValue
	);
	*/

	StringPrintFormatSafe(
		csResponse,
		sizeof(csResponse) - 1,
		"https://%s/%s/%s",
		lpfdServer->lpszValue,
		lpfdGroup->lpszValue,
		lpfdProject->lpszValue
	);

	/* Process Name */
	StringPrintFormatSafe(
		csProcess,
		sizeof(csProcess) - 1,
		"./Cookies/%s.sh",
		lpfdCookie->lpszValue
	);

	/* Process Args */
	StringPrintFormatSafe(
		csProcessArgs,
		sizeof(csProcessArgs) - 1,
		"--license \"%s\" "
		"--server \"%s\" "
		"--group \"%s\" "
		"--project \"%s\" "
		"--cookie \"%s\" "
		"--description \"%s\" "
		"--readme \"%s\"",
		lpfdLicense->lpszValue,
		lpfdServer->lpszValue,
		lpfdGroup->lpszValue,
		lpfdProject->lpszValue,
		lpfdCookie->lpszValue,
		lpfdDescription->lpszValue,
		lpfdReadMe->lpszValue
	);

	PrintFormat("Args: %s\n", csProcessArgs);

	/* If DEBUG Build, Write Out Cookie Script To STDOUT */
	#ifdef __DEBUG__
	hProcess = CreateProcess(
		csProcess,
		csProcessArgs,
		TRUE	
	);
	#else 
	hProcess = CreateProcess(
		csProcess,
		csProcessArgs,
		FALSE
	);
	#endif

	if (NULL_OBJECT == hProcess)
	{ return HTTP_STATUS_500_INTERNAL_SERVER_ERROR; }

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroyObject(hProcess);

	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");
	RequestAddResponse(lprdData->hRequest, csResponse, StringLength(csResponse));


	return HTTP_STATUS_200_OK;
}




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE
__ProjectJson(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hProcess;
	json_t *lpjPayload;
	json_t *lpjLicense;
	json_t *lpjServer;
	json_t *lpjGroup;
	json_t *lpjProject;
	json_t *lpjCookie;
	json_t *lpjDescription;
	json_t *lpjReadMe;
	json_error_t jeError;
	LPSTR lpszLicense;
	LPSTR lpszServer;
	LPSTR lpszGroup;
	LPSTR lpszProject;
	LPSTR lpszCookie;
	LPSTR lpszDescription;
	LPSTR lpszReadMe;
	UARCHLONG ualIndex;
	CSTRING csResponse[512];
	CSTRING csProcess[512];
	CSTRING csProcessArgs[8192];

	ZeroMemory(csResponse, sizeof(csResponse));
	ZeroMemory(csProcess, sizeof(csProcess));
	ZeroMemory(csProcessArgs, sizeof(csProcessArgs));

	if (FALSE != PostIsMultipartForm(lprdData->hRequest))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	if (NULLPTR == lprdData->lpRequestBody)
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	lpjPayload = json_loads(lprdData->lpRequestBody, 0, &jeError);
	lpjLicense = json_object_get(lpjPayload, "License");
	lpjServer = json_object_get(lpjPayload, "Server");
	lpjGroup = json_object_get(lpjPayload, "Group");
	lpjProject = json_object_get(lpjPayload, "Project");
	lpjCookie = json_object_get(lpjPayload, "Cookie");
	lpjDescription = json_object_get(lpjPayload, "Description");
	lpjReadMe = json_object_get(lpjReadMe, "ReadMe");

	lpszLicense = (LPSTR)json_string_value(lpjLicense);
	lpszServer = (LPSTR)json_string_value(lpjServer);
	lpszGroup = (LPSTR)json_string_value(lpjGroup);
	lpszProject = (LPSTR)json_string_value(lpjProject);
	lpszCookie = (LPSTR)json_string_value(lpjCookie);
	lpszDescription = (LPSTR)json_string_value(lpjDescription);
	lpszReadMe = (LPSTR)json_string_value(lpjReadMe);

	if (
		NULLPTR == lpszLicense ||
		NULLPTR == lpszServer ||
		NULLPTR == lpszGroup ||
		NULLPTR == lpszProject ||
		NULLPTR == lpszCookie ||
		NULLPTR == lpszDescription ||
		NULLPTR == lpszReadMe
	){ 
		json_decref(lpjPayload);
		return HTTP_STATUS_400_BAD_REQUEST;
	}
	
	/* Is Cookie Path Safe? */
	if (FALSE == __CookieIsSafe((LPCSTR)lpszCookie))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	StringPrintFormatSafe(
		csResponse,
		sizeof(csResponse) - 1,
		"{\"url\": \"https://%s/%s/%s\"}",
		lpszServer,
		lpszGroup,
		lpszProject
	);

	/* Process Name */
	StringPrintFormatSafe(
		csProcess,
		sizeof(csProcess) - 1,
		"./Cookies/%s.sh",
		lpszCookie
	);

	/* Process Args */
	StringPrintFormatSafe(
		csProcessArgs,
		sizeof(csProcessArgs) - 1,
		"--license \"%s\" "
		"--server \"%s\" "
		"--group \"%s\" "
		"--project \"%s\" "
		"--cookie \"%s\" "
		"--description \"%s\" "
		"--readme \"%s\"",
		lpszLicense,
		lpszServer,
		lpszGroup,
		lpszProject,
		lpszCookie,
		lpszDescription,
		lpszReadMe
	);

	#ifdef __DEBUG__
	hProcess = CreateProcess(
		csProcess,
		csProcessArgs,
		TRUE
	);
	#else 
	hProcess = CreateProcess(
		csProcess,
		csProcessArgs,
		FALSE
	);
	#endif

	if (NULL_OBJECT == hProcess)
	{ return HTTP_STATUS_500_INTERNAL_SERVER_ERROR; }

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroyObject(hProcess);

	RequestAddHeader(lprdData->hRequest, "Content-Type: application/json");
	RequestAddResponse(lprdData->hRequest, csResponse, StringLength(csResponse));

	json_decref(lpjPayload);
	return HTTP_STATUS_200_OK;
}




_Success_(return == 0, _Non_Locking_)
LONG 
Main(
	_In_ 			LONG 			lArgCount,
	_In_Z_ 			DLPSTR 			dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	STATE stProgram;

	stProgram.dlpszBindAddresses = GlobalAllocAndZero(sizeof(LPSTR));
	stProgram.lpusBindPorts = GlobalAllocAndZero(sizeof(USHORT));

	stProgram.dlpszBindAddresses[0] = "0.0.0.0";
	stProgram.lpusBindPorts[0] = 9998;


	stProgram.hHttpServer = CreateHttpServerEx(
		"CookieCutter",
		(DLPCSTR)stProgram.dlpszBindAddresses,
		1,
		stProgram.lpusBindPorts,
		1,
		"cookiecutter",
		16384,
		4,
		0
	);

	if (NULL_OBJECT == stProgram.hHttpServer)
	{ PostQuitMessage(1); }


	DefineHttpUriProc(
		stProgram.hHttpServer,
		"/",
		HTTP_METHOD_GET,
		__Root,
		&stProgram
	);
	
	
	DefineHttpUriProc(
		stProgram.hHttpServer,
		"/Project",
		HTTP_METHOD_POST,
		__Project,
		&stProgram
	);
	
	
	DefineHttpUriProc(
		stProgram.hHttpServer,
		"/ProjectJson",
		HTTP_METHOD_POST,
		__ProjectJson,
		&stProgram
	);


	INFINITE_LOOP()
	{ LongSleep(INFINITE_WAIT_TIME); }

	return 0;
}