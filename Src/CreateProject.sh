#!/usr/bin/bash

#   ____            _    _       ____      _   _            
#  / ___|___   ___ | | _(_) ___ / ___|   _| |_| |_ ___ _ __ 
# | |   / _ \ / _ \| |/ / |/ _ \ |  | | | | __| __/ _ \ '__|
# | |__| (_) | (_) |   <| |  __/ |__| |_| | |_| ||  __/ |   
# \____\___/ \___/|_|\_\_|\___|\____\__,_|\__|\__\___|_|  

# Tool Suite For Creating Project Templates.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


INSTANCE="http://127.0.0.1:9998"

echo "Project Name? "
read PROJECT 
echo "Group (username or group)? "
read GROUP 
echo "Cookie? "
read COOKIE 
echo "Description? "
read DESCRIPTION 
echo "Server (gitlab.com)? "
read SERVER 
echo "License (AGPLv3)? "
read LICENSE 
echo "ReadMe (AGPLv3_Normal)? "
read README 


URL=$(curl -X POST "$INSTANCE/Project" 	\
	-F "Project=$PROJECT"		\
	-F "Group=$GROUP"		\
	-F "Cookie=$COOKIE"		\
	-F "Description=$DESCRIPTION"	\
	-F "Server=$SERVER"		\
	-F "License=$LICENSE"		\
	-F "ReadMe=$README")

echo "$URL"