# <<<PROJECT>>>

<<<DESCRIPTION>>>

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
<<<LICENSE_CAT>>>
```

<<<PROJECT>>> is licensed under the AGPLv3 license. Don't like it? Go else where.


## Donate
Like <<<PROJECT>>>? You use it yourself, or for your infrastructure? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59

## Docker
There will be a pre-built Docker image in the future.
