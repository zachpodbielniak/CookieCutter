#   ____            _    _       ____      _   _            
#  / ___|___   ___ | | _(_) ___ / ___|   _| |_| |_ ___ _ __ 
# | |   / _ \ / _ \| |/ / |/ _ \ |  | | | | __| __/ _ \ '__|
# | |__| (_) | (_) |   <| |  __/ |__| |_| | |_| ||  __/ |   
# \____\___/ \___/|_|\_\_|\___|\____\__,_|\__|\__\___|_|  

# Tool Suite For Creating Project Templates.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


ITERATOR=0
ARGS=("$@")

SERVER=""
GROUP=""
LICENSE=""
LHEADER=""
BANNER=""
PROJECT=""
COOKIE=""
DESCRIPTION=""
BANNER=""
APITOKEN=""



if [ $# -lt 1 ]
then
	echo "Use this tool correctly"
	echo "--help to see how to use it"
	exit 1
fi


# Main Loop
while [ $ITERATOR -lt $# ]
do

	# Populate KEY And VALUE
	KEY=${ARGS[$ITERATOR]}
	if [ $ITERATOR -lt $(( $#-1 )) ]
	then 
		VALUE=${ARGS[$(( ITERATOR+1 ))]}
	else 
		VALUE=""
	fi


	# Help
	if [ "--help" = "$KEY" ] || [ "-l" = "$KEY" ]
	then
		echo -e "CookieCutter -- A Project Template Creator Written In C"
		echo -e "CookieCutter Is Released Under The AGPLv3 License"
		echo -e "\t-h,--help\t\tShow this info"
		echo -e "\t-l,--license\t\tLicense for new project"
		echo -e "\t-s,--server\t\tServer info (gitlab.com)"
		echo -e "\t-g,--group\t\tGroup info (username, or group)"
		echo -e "\t-p,--project\t\tProject name"
		echo -e "\t-c,--cookie\t\tThe cookie to use for the template"
		echo -e "\t-d,--description\tDescription for the project"
		echo -e "\t-r,--readme\t\tReadme to use"
		exit 0
	fi


	# Set License 
	if [ "--license" = "$KEY" ] || [ "-l" = "$KEY" ]
	then
		LICENSE="$VALUE"
		LHEADER="$VALUE"
		LHEADER+="_Header"
	fi
	
	
	# Set Server
	if [ "--server" = "$KEY" ] || [ "-s" = "$KEY" ]
	then
		SERVER="$VALUE"
	fi
	
	
	# Set Group
	if [ "--group" = "$KEY" ] || [ "-g" = "$KEY" ]
	then
		GROUP="$VALUE"
	fi
	
	
	# Set Project
	if [ "--project" = "$KEY" ] || [ "-p" = "$KEY" ]
	then
		PROJECT="$VALUE"
		BANNER=$(figlet $VALUE)
	fi
	
	
	# Set Cookie
	if [ "--cookie" = "$KEY" ] || [ "-c" = "$KEY" ]
	then
		COOKIE="$VALUE"
	fi
	
	
	# Set Description
	if [ "--description" = "$KEY" ] || [ "-d" = "$KEY" ]
	then
		DESCRIPTION="$VALUE"
	fi
	
	
	# Set Description
	if [ "--readme" = "$KEY" ] || [ "-r" = "$KEY" ]
	then
		README="$VALUE"
	fi


	# Increment
	ITERATOR=$(( ITERATOR+1 ))
done


# API-Token
APITOKEN=$(cat ~/.config/gitlab)


##################
# The Pre-Cookie #
##################


mkdir -p "./Projects/"
cd "./Projects/"

# GitLab
#URL="https://gitlab.com/api/v4/projects?name=$PROJECT&visibility=public&description=$DESCRIPTION"
URL="https://gitlab.com/api/v4/projects?name=$PROJECT&path=$PROJECT&visibility=public"
curl -s -X POST --header "Private-Token: $APITOKEN" "$URL" > /dev/null 2>&1
sh -c "git clone git@$SERVER:$GROUP/$PROJECT.git"

cd "./$PROJECT"

cp "../../Licenses/$LICENSE" ./LICENSE
echo -e "$BANNER\n\n" > ./LHEADER
cat "../../Licenses/$LHEADER" >> ./LHEADER
cp "../../ReadMes/$README" ./README.md


sed -i -e "s/<<<DESCRIPTION>>>/$DESCRIPTION/g" ./LICENSE
sed -i -e "s/<<<AUTHOR>>>/$AUTHOR/g" ./LICENSE 
sed -i -e "s/<<<YEAR>>>/$(date +%Y)/g" ./LICENSE

sed -i -e "s/<<<DESCRIPTION>>>/$DESCRIPTION/g" ./LHEADER
sed -i -e "s/<<<AUTHOR>>>/$AUTHOR/g" ./LHEADER
sed -i -e "s/<<<YEAR>>>/$(date +%Y)/g" ./LHEADER

sed -i -e "s/<<<DESCRIPTION>>>/$DESCRIPTION/g" ./README.md
sed -i -e "s/<<<PROJECT>>>/$PROJECT/g" ./README.md


##############
# The Cookie #
##############

# Make Octothorpe Commented Header
cp ./LHEADER ./LHEADEROCTO
sed -i -e 's/^/#\ /g' ./LHEADEROCTO

# Make Dirs
mkdir -p "./Src"

# Make Script
SCRIPT="#!/usr/bin/bash"
SCRIPT+="\n\n"
SCRIPT+=$(cat "./LHEADEROCTO")
SCRIPT+="\n\n\n"
SCRIPT+="#Your Code Goes Here"
echo -e "$SCRIPT" > "./Src/$PROJECT.sh"


# Make Dockerfile
DOCKER=$(cat "./LHEADEROCTO")
DOCKER+="\n\n\n"
DOCKER+="FROM zachpodbielniak/podnet:latest\n\n"
DOCKER+="WORKDIR /$PROJECT\n"
DOCKER+="COPY . .\n"
DOCKER+="\n"
DOCKER+="CMD [\"./Src/$PROJECT.sh\"]\n"
echo -e "$DOCKER" > "Dockerfile"

rm ./LHEADEROCTO


###################
# The Post-Cookie #
###################

rm LHEADER
git add -A 
git commit -m "Initial Commit"
git push
cd ../../