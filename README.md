# CookieCutter

Tool Suite For Creating Project Templates.

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
  ____            _    _       ____      _   _            
 / ___|___   ___ | | _(_) ___ / ___|   _| |_| |_ ___ _ __ 
| |   / _ \ / _ \| |/ / |/ _ \ |  | | | | __| __/ _ \ '__|
| |__| (_) | (_) |   <| |  __/ |__| |_| | |_| ||  __/ |   
 \____\___/ \___/|_|\_\_|\___|\____\__,_|\__|\__\___|_|  

Tool Suite For Creating Project Templates.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

CookieCutter is licensed under the AGPLv3 license. Don't like it? Go else where.

CookieCutter utilizes my [CRock](https://gitlab.com/zachpodbielniak/CRock) library, which is a way to write WebAPI's in C, and my [PodNet](https://gitlab.com/zachpodbielniak/PodNet) library, which is a general purpose C library for making life easier.



## Donate
Like CookieCutter? You use it yourself, or for your infrastructure? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59

## Docker
There will be a pre-built Docker image in the future. As of right now, just build the one in this repo.

```
docker build -t cookiecutter .
docker run -d --name cookiecutter cookiecutter
curl http://172.17.0.2:9998/
```