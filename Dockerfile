#   ____            _    _       ____      _   _            
#  / ___|___   ___ | | _(_) ___ / ___|   _| |_| |_ ___ _ __ 
# | |   / _ \ / _ \| |/ / |/ _ \ |  | | | | __| __/ _ \ '__|
# | |__| (_) | (_) |   <| |  __/ |__| |_| | |_| ||  __/ |   
# \____\___/ \___/|_|\_\_|\___|\____\__,_|\__|\__\___|_|  

# Tool Suite For Creating Project Templates.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


FROM zachpodbielniak/crock:latest

RUN pacman -S --noconfirm jansson openssh figlet
RUN useradd -rm -d /home/zach -s /bin/bash -g root -u 1000 zach

WORKDIR /home/zach/cookiecutter
COPY . .

RUN make -j$(nproc)
RUN chown -R zach ../cookiecutter

USER zach
RUN git config --global user.email "z.podbielniak@disroot.org"
RUN git config --global user.name "Zach Podbielniak"

EXPOSE 9998
CMD ["./bin/cookiecutter"]